EJERCICIO INTEGRADOR (a Pensar en Clase)

La idea es hacer un ejercicio Integrador de algunos conceptos vistos hasta ahora.
Hacer el programa de un DISPOSITIVO ELECTRONICO que simule el control de Temperatura de una Caldera
Convierta los valores de temperatura y envie una alarma a otro sistema.

1. FUNCION: Hacer un Algoritmo dentro de una funcion llamada CelTOFah que convierta grados CELSIUS EN FAHRENHEIT
2. En el cuerpo principal del programa, solicitar el ingreso de 10 valores de Temperatura en Grados Celsius (enteros)
y guardarlos en un vector.
3. Ordenar los valores de Temperatura de Menor a Mayor, Utilizando algoritmo de Burbuja que este en una funcion llamada ORDENAR
4. En un nuevo Vector, guardar los valores de temperatura pero en grados Fahrenheit
5. Finalmente, Guardar los valores de temperatura en un Archivo de Texto.