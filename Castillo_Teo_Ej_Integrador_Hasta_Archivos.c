/*4. En un nuevo Vector, guardar 
los valores de temperatura pero en grados Fahrenheit*/
#include <stdio.h>
#include <conio.h>
#define CANT_TEMPERATURAS 10
void ORDENAR();
int CelTOFah ();

int main () {
    int celcius[CANT_TEMPERATURAS];
    int fahrenheit[CANT_TEMPERATURAS];
    char temperatura;
    int i;
    FILE* fichero;
    
    for (i = 0; i < CANT_TEMPERATURAS; i++)
    {
        printf("\nIngrese la %dº temperatura: ", i+1);
        scanf("%d", &celcius[i]);
    }
    
    ORDENAR(celcius);
    
    fichero = fopen("Temperaturas.txt ","a+");
    if (fichero == NULL) 
    {
        printf( "No se puede abrir el fichero.\n" );
        exit( 1 );
    }

    fprintf(fichero, "Temperaturas en un mismo inicio:\n");
    for ( i = 0; i < CANT_TEMPERATURAS; i++)
    {
        fahrenheit[i] = CelTOFah(celcius[i]);
        fprintf(fichero, "%dFº\n", fahrenheit[i]);
    }

    fputc('\n',fichero);
    fclose(fichero);
    getch();
    return 0;
}

void ORDENAR (int vector[])
{
    int i, j, aux;

    for (i = 0; i < CANT_TEMPERATURAS-1; i++)
    {
        for (j = 0; j < CANT_TEMPERATURAS - 1 - i; j++)
        {
            if (vector[j] > vector[j+1])
            {
                aux = vector[j+1];
                vector[j+1] = vector[j];
                vector[j] = aux;
            }
        }
        
    }
    
}

int CelTOFah (int celcius)
{
    int fahrenheit;

    fahrenheit=(celcius * 9/5) + 32;
    return fahrenheit;
}
